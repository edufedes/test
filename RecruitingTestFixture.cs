﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace RecruitingExercise
{
    public class RecruitingTestFxiture : IDisposable
    {
        public readonly IServiceProvider Provider;

        public RecruitingTestFxiture()
        {
            // var configuration = new ConfigurationBuilder().Build();

            var services = new ServiceCollection();

            // services.AddScoped<ITestBusiness, ExcerciseBusiness>()
            // services.AddScoped<TestController>()

            // services.AddSingleton(mockTestDao.Object)

            Provider = services.BuildServiceProvider();
        }

        public void Dispose()
        {
            ;
        }
    }
}
