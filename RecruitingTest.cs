﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace RecruitingExercise
{
    public class RecruitingTest : IClassFixture<RecruitingTestFxiture>
    {
        private readonly RecruitingTestFxiture _fixture;

        public RecruitingTest(RecruitingTestFxiture fixture, ITestOutputHelper helper)
        {
            _fixture = fixture;

            // helper.SetOutputHelper();
        }

        [Fact]
        public void TestYourEnvironment()
        {
            Assert.True(true, "Test Your Environment");
        }

        [Fact]
        public void TestYourEnvironment_WithFluentAssertions()
        {
            var testVariable = (2 + 2) == 4;
            testVariable.Should().BeTrue();
        }

        [Fact]
        public async Task Get_TestBusiness_DefaultBusinessDataReturned()
        {
            using (var scope = _fixture.Provider.CreateScope())
            {
                // var testController = scope.ServiceProvider.GetRequiredService<TestController>();
                // var result = await testController.GetDefaultBusinessDataAsync();

                // Assert.Equal(result.Id, defaultId);
            }

            Assert.True(true);
        }
    }
}
