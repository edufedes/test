## Introduction

This is a .NET Core application for a testing purposes, integrated with the pipeline to run the project Unit Tests.

## Configuration comments

The `.gitlab-ci.yml` contains the configuration needed for GitLab to build the code, run the Unit Tests and prepared to complete the configuration for future deployment wit three defined stages here: `build`, and `test` and `deploy`.

```
stages:
    - build
    - test
    - deploy
```

Build configuration to run `dotnet build` command and identify the `bin` folder as the output directory. 
Anything in the `bin` folder will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

We get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```

## Developing with Gitpod

This template repository also has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

The `.gitpod.yml` ensures that, when you open this repository in Gitpod, you'll get a cloud workspace with .NET Core pre-installed, and your project will automatically be built and start running.
